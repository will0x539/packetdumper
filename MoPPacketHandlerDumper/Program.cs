﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using Magic;

namespace MoPPacketHandlerDumper
{
    /// <summary>
    /// Program main class
    /// </summary>
    class Program
    {
        /// <summary>
        /// Packet structure
        /// </summary>
        public struct Packet
        {
            public short Opcode;
            public short Length;
            public byte[] PacketBytes;
        };

        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////

        /*
         *  Functions to hooks addr 
         */
        private const uint Addr_CallPacketHandler           = 0x03F8B15;
        private const uint Addr_NetClient__ProcessMessage   = 0x3F8A80;
        private const uint Addr_NetClient__Send2            = 0x3FAE30;

        /*
         * Hook lib
         */
        private static BlackMagic BlackMagic;
        
        /*
         * Hooks handle
         */
        private static GEHook.Hook Handle_Hook_CallPacketHandler;
        private static GEHook.Hook Handle_Hook_NetClient__ProcessMessage;
        private static GEHook.Hook Handle_Hook_NetClient__Send2;

        /*
         * Packet queue
         */
        private static List<Packet> Packets = new List<Packet>();

        /*
         * Log path
         */
        private static string LogPath = String.Empty;

        /*
         * Known opcodes
         */
        private static Dictionary<short, string> KnownOpcodes = new Dictionary<short, string>();

        /*
         * Ignore opcodes
         */
        private static List<short> IgnoreOpcodes = new List<short>();

        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////

        static void Main(string[] args)
        {
            LogPath = "Dump_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second + ".log";

            /*
             * Load known opcode
             */
            using (TextReader lReader = File.OpenText("KnownOpcodes.txt"))
            {
                string lLine;
                while ((lLine = lReader.ReadLine()) != null)
                {
                    string[] lBits = lLine.Split('=');
                    KnownOpcodes.Add((short)FromHex(lBits[1]), lBits[0]);
                }
            }

            /*
             * Load ignore opcode
             */
            using (TextReader lReader = File.OpenText("IgnoreOpcodes.txt"))
            {
                string lLine;
                while ((lLine = lReader.ReadLine()) != null)
                    IgnoreOpcodes.Add((short)FromHex(lLine));
            }

            /*
             * Init hook lib (BlackMagic, GeHook)
             */
            BlackMagic = new Magic.BlackMagic();
            BlackMagic.OpenProcessAndThread(SProcess.GetProcessFromWindowTitle("World of Warcraft"));

            GEHook.Controller.Initialize(BlackMagic.ProcessId);

            /*
             * Init hooks
             */
            Handle_Hook_CallPacketHandler           = GEHook.Controller.CreateAndApplyHook((uint)BlackMagic.MainModule.BaseAddress + Addr_CallPacketHandler,            0x0A,   "Hook_CallPacketHandler");
            Handle_Hook_NetClient__ProcessMessage   = GEHook.Controller.CreateAndApplyHook((uint)BlackMagic.MainModule.BaseAddress + Addr_NetClient__ProcessMessage,    0x0A,   "NetClient__ProcessMessage");
            Handle_Hook_NetClient__Send2            = GEHook.Controller.CreateAndApplyHook((uint)BlackMagic.MainModule.BaseAddress + Addr_NetClient__Send2,             0x06,   "NetClient__Send2");

            /*
             * Init call back
             */
            Handle_Hook_CallPacketHandler.OnHookReached         += new GEHook.Hook.OnHookReachedEventHandler(Hook_CallPacketHandler_OnHookReached);
            Handle_Hook_NetClient__ProcessMessage.OnHookReached += new GEHook.Hook.OnHookReachedEventHandler(Hook_NetClient__ProcessMessage_OnHookReached);
            Handle_Hook_NetClient__Send2.OnHookReached          += new GEHook.Hook.OnHookReachedEventHandler(Hook_NetClient__Send2_OnHookReached);

            Console.WriteLine("Hooked :)");

            Console.ReadKey();
            GEHook.Controller.DisposeAndRemoveAllExecutors();
            GEHook.Controller.DisposeAndRemoveAllHooks();
            Process.GetCurrentProcess().Kill();

        }

        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// On client call a message handler
        /// </summary>
        /// <param name="pArgs">Hook context</param>
        static void Hook_CallPacketHandler_OnHookReached(GEHook.Hook.OnHookReachedEventArgs pArgs)
        {
            short   lOpcode     = (short)pArgs.Registers.ESI;
            uint    lHandler    = ((uint)pArgs.Registers.ECX) - (uint)BlackMagic.MainModule.BaseAddress + 0x400000;

            foreach (Packet lPacket in Packets)
            {
                if (lPacket.Opcode == lOpcode)
                {
                    ReportCompletePacket(true, lPacket, lHandler);
                    Packets.Remove(lPacket);

                    return;
                }
            }

            ReportPacketHeader(true, lOpcode, lHandler);
        }
        /// <summary>
        /// On client receive a message
        /// </summary>
        /// <param name="pArgs">Hook context</param>
        static void Hook_NetClient__ProcessMessage_OnHookReached(GEHook.Hook.OnHookReachedEventArgs pArgs)
        {
            uint lDataStorePtr  = BlackMagic.ReadUInt(pArgs.Registers.EBP + 0xC);
            uint lPacketDataPtr = BlackMagic.ReadUInt(lDataStorePtr + 0x4);

            Packet lHeader      = new Packet();
            lHeader.Opcode      = BlackMagic.ReadShort(lPacketDataPtr);
            lHeader.Length      = BlackMagic.ReadShort(lDataStorePtr + 0x10);
            lHeader.PacketBytes = BlackMagic.ReadBytes(lPacketDataPtr, lHeader.Length);

            Packets.Add(lHeader);
        }
        /// <summary>
        /// On client send a message
        /// </summary>
        /// <param name="pArgs">Hook context</param>
        static void Hook_NetClient__Send2_OnHookReached(GEHook.Hook.OnHookReachedEventArgs pArgs)
        {
            uint lDataStorePtr  = BlackMagic.ReadUInt(pArgs.Registers.EBP + 0x8);
            uint lPacketDataPtr = BlackMagic.ReadUInt(lDataStorePtr + 0x4);

            Packet lPacket      = new Packet();
            lPacket.Opcode      = BlackMagic.ReadShort(lPacketDataPtr);
            lPacket.Length      = BlackMagic.ReadShort(lDataStorePtr + 0x10);
            lPacket.PacketBytes = BlackMagic.ReadBytes(lPacketDataPtr, lPacket.Length);

            ReportCompletePacket(false, lPacket, 0);    
        }

        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Report a complete packet (Header + Data + Handler)
        /// </summary>
        /// <param name="pRecv">Is a received packet</param>
        /// <param name="pPacket">Packet</param>
        /// <param name="pHandler">Packet handler on client side</param>
        public static void ReportCompletePacket(bool pRecv, Packet pPacket, uint pHandler)
        {
            if (IgnoreOpcodes.Contains(pPacket.Opcode))
                return;

            String lReportHeader;

            if (pRecv)
                lReportHeader = "[Receive] -> ";
            else
                lReportHeader = "[Send] -> ";

            if (KnownOpcodes.ContainsKey(pPacket.Opcode))
                lReportHeader = lReportHeader + "[" + KnownOpcodes[pPacket.Opcode] + "] ";
            else
                lReportHeader = lReportHeader + "[UNKNOWN] ";

            lReportHeader = lReportHeader + "[" + DateTime.Now.ToLongTimeString() + "] Opcode : 0x" + pPacket.Opcode.ToString("X4") + "(" + pPacket.Opcode + ") -> Handler  0x" + pHandler.ToString("X8") + " Size : " + pPacket.Length;

            String lHexDumpStr = HexDump(pPacket.PacketBytes);
            
            System.Console.WriteLine(lReportHeader);
            System.IO.File.AppendAllText(LogPath, lReportHeader + Environment.NewLine + lHexDumpStr + Environment.NewLine + Environment.NewLine);
        }
        /// <summary>
        /// Report a packet header
        /// </summary>
        /// <param name="pRecv">Is a received packet</param>
        /// <param name="pOpcode">Packet opcode</param>
        /// <param name="pHandler">Packet handler on client side</param>
        public static void ReportPacketHeader(bool pRecv, short pOpcode, uint pHandler)
        {
            if (IgnoreOpcodes.Contains(pOpcode))
                return;

            String lReportHeader;

            if (pRecv)
                lReportHeader = "[Receive] -> ";
            else
                lReportHeader = "[Send] -> ";

            if (KnownOpcodes.ContainsKey(pOpcode))
                lReportHeader = lReportHeader + "[" + KnownOpcodes[pOpcode] + "] ";
            else
                lReportHeader = lReportHeader + "[UNKNOWN] ";

            lReportHeader = lReportHeader + "[" + DateTime.Now.ToLongTimeString() + "] Opcode : 0x" + pOpcode.ToString("X4") + "(" + pOpcode + ") -> Handler  0x" + pHandler.ToString("X8");

            System.Console.WriteLine(lReportHeader);
            System.IO.File.AppendAllText(LogPath, lReportHeader + Environment.NewLine);
        }

        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Make a HexDump of a byte array
        /// </summary>
        /// <param name="pBytes">Input array</param>
        /// <returns>A string contenting the hex dump</returns>
        public static string HexDump(byte[] pBytes)
        {
            if (pBytes == null) 
                return "<null>";

            int lLen    = pBytes.Length;
            var lResult = new StringBuilder(((lLen + 15) / 16) * 78);
            var lChars  = new char[78];

            for (int lI = 0; lI < 75; lI++)
                lChars[lI] = ' ';

            lChars[76] = '\r';
            lChars[77] = '\n';

            for (int lI2 = 0; lI2 < lLen ; lI2 += 16)
            {
                lChars[0] = HexChar(lI2 >> 28);
                lChars[1] = HexChar(lI2 >> 24);
                lChars[2] = HexChar(lI2 >> 20);
                lChars[3] = HexChar(lI2 >> 16);
                lChars[4] = HexChar(lI2 >> 12);
                lChars[5] = HexChar(lI2 >> 8);
                lChars[6] = HexChar(lI2 >> 4);
                lChars[7] = HexChar(lI2 >> 0);

                int lOffset1 = 11;
                int lOffset2 = 60;

                for (int lI3 = 0; lI3 < 16; lI3++)
                {
                    if (lI2 + lI3 >= lLen)
                    {
                        lChars[lOffset1] = ' ';
                        lChars[lOffset1 + 1] = ' ';
                        lChars[lOffset2] = ' ';
                    }
                    else
                    {
                        byte lB = pBytes[lI2 + lI3];

                        lChars[lOffset1]        = HexChar(lB >> 4);
                        lChars[lOffset1 + 1]    = HexChar(lB);
                        lChars[lOffset2]        = (lB < 32 ? '·' : (char)lB);
                    }

                    lOffset1 += (lI3 == 7 ? 4 : 3);
                    lOffset2++;
                }

                lResult.Append(lChars);
            }

            return lResult.ToString();
        }
        /// <summary>
        /// Make a hex value for a char
        /// </summary>
        /// <param name="pValue">Input value</param>
        /// <returns>The converted value</returns>
        private static char HexChar(int pValue)
        {
            pValue &= 0xF;

            if (pValue >= 0 && pValue <= 9)
                return (char)('0' + pValue);

            return (char)('A' + (pValue - 10));
        }
        /// <summary>
        /// Convert a hex string to number
        /// </summary>
        /// <param name="pValue">Input (string)</param>
        /// <returns>Output (integer)</returns>
        public static int FromHex(string pValue)
        {
            if (pValue.StartsWith("0x", StringComparison.OrdinalIgnoreCase))
                pValue = pValue.Substring(2);

            return Int32.Parse(pValue, System.Globalization.NumberStyles.HexNumber);
        }

    }
}
